#include <errno.h>
#include <string.h>

#include "image_bmp.h"

#define BMP_MAGIC 0x4d42
#define BMP_BI_SIZE 40
#define BMP_BI_PLANES 1
#define BMP_BI_BIT_COUNT 24

struct __attribute__((packed)) bmp_header  {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static enum bmp_read_status validate_header(struct bmp_header *header) {
    if (header->bfType != BMP_MAGIC)
        return BMP_READ_ERROR_INVALID_SIGNATURE;
    
    if (header->biPlanes != 1)
        return BMP_READ_ERROR_UNSUPPORTED;

    if (header->biBitCount != 24)
        return BMP_READ_ERROR_UNSUPPORTED;

    if (header->biCompression != 0)
        return BMP_READ_ERROR_UNSUPPORTED;

    return BMP_READ_OK;
}

static size_t compute_padding(size_t width) {
    return width % 4 == 0 ? 0 : 4 - (width * 3) % 4;
}

char *bmp_read_status_message(enum bmp_read_status status) {
    switch (status) {
    case BMP_READ_OK:
        return "OK";
    case BMP_READ_ERROR_IO:
        return strerror(errno);
    case BMP_READ_ERROR_OOM:
        return "Out of memory";
    case BMP_READ_ERROR_INVALID_SIGNATURE:
        return "Invalid BMP signature";
    case BMP_READ_ERROR_UNSUPPORTED:
        return "Unsupported BMP";
    case BMP_READ_ERROR_UNEXPECTED_EOF:
        return "Unexpected EOF";
    default:
        return "Unknown error";
    }
}

enum bmp_read_status from_bmp(FILE *file, struct image *image) {
    struct bmp_header header;
    enum bmp_read_status status;
    
    if (fread(&header, sizeof(struct bmp_header), 1, file) != 1)
        return BMP_READ_ERROR_IO;

    if ((status = validate_header(&header)) != BMP_READ_OK)
        return status;

    if (fseek(file, header.bOffBits, SEEK_SET) != 0)
        return BMP_READ_ERROR_IO;

    image_init(image, header.biWidth, header.biHeight);
    if (image->data == NULL)
        return BMP_READ_ERROR_OOM;

    size_t padding = compute_padding(header.biWidth);
    struct pixel *data = image->data;
    
    for (uint32_t y = 0; y < header.biHeight; y++) {
        if (y == header.biHeight - 1) padding = 0;

        size_t count = header.biWidth * 3 + padding;
        if (fread(data, 1, count, file) != count)
            return BMP_READ_ERROR_UNEXPECTED_EOF;

        data += header.biWidth;
    }

    return BMP_READ_OK;
}

enum bmp_read_status from_bmp_path(char *path, struct image *image) {
    FILE *file = fopen(path, "rb");
    if (file == NULL)
        return BMP_READ_ERROR_IO;
    enum bmp_read_status status = from_bmp(file, image);
    fclose(file);
    return status;
}

char *bmp_write_status_message(enum bmp_write_status status) {
    switch (status) {
    case BMP_WRITE_OK:
        return "OK";
    case BMP_WRITE_ERROR_IO:
        return strerror(errno);
    default:
        return "Unknown error";
    }
}

enum bmp_write_status to_bmp(FILE *file, struct image const* image) {
    size_t padding = compute_padding(image->width);
    struct pixel *data = image->data;

    size_t image_size = (image->width * 3 + padding) * image->height;

    struct bmp_header header = {
        .bfType = BMP_MAGIC,
        .bfileSize = sizeof(struct bmp_header) + image_size,
        .bfReserved = 0,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = BMP_BI_SIZE,
        .biWidth = image->width,
        .biHeight = image->height,
        .biPlanes = BMP_BI_PLANES,
        .biBitCount = BMP_BI_BIT_COUNT,
        .biCompression = 0,
        .biSizeImage = image_size,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0,
    };
    
    if (fwrite(&header, sizeof(struct bmp_header), 1, file) != 1)
        return BMP_WRITE_ERROR_IO;

    for (uint32_t y = 0; y < header.biHeight; y++) {
        if (y == header.biHeight - 1) padding = 0;

        size_t count = header.biWidth * 3 + padding;
        if (fwrite(data, 1, count, file) != count)
            return BMP_WRITE_ERROR_IO;


        data += header.biWidth;
    }

    return BMP_WRITE_OK;
}

enum bmp_write_status to_bmp_path(char *path, struct image const* image) {
    FILE *file = fopen(path, "wb");
    if (file == NULL)
        return BMP_WRITE_ERROR_IO;
    enum bmp_write_status status = to_bmp(file, image);
    fclose(file);
    return status;
}
