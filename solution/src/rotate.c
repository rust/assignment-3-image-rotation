#include "rotate.h"
#include <stdio.h>

struct image rotate(struct image const* source) {
    struct image result;
    image_init(&result, source->height, source->width);
    if (result.data == NULL) return result;
    
    for (uint64_t x = 0; x < source->width; x++) {
        for (uint64_t y = 0; y < source->height; y++) {
            image_set(&result, source->height - y - 1, x, image_get(source, x, y));
        }
    }
    return result;
}
