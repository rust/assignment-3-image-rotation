#include <stdio.h>
#include <stdlib.h>

#include "image.h"

void image_init(struct image *image, uint64_t width, uint64_t height) {
    image->width = width;
    image->height = height;
    image->data = calloc(width * height, sizeof(struct pixel));
}

void image_free(struct image *image) {
    free(image->data);
    image->data = NULL;
}

struct pixel image_get(struct image const* image, uint64_t x, uint64_t y) {
    return image->data[image->width * y + x];
}

void image_set(struct image *image, uint64_t x, uint64_t y, struct pixel pixel) {
    image->data[image->width * y + x] = pixel;
}
