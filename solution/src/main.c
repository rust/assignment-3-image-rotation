#include <stdio.h>
#include <stdlib.h>

#include "image_bmp.h"
#include "rotate.h"

int main(int argc, char **argv) {
    int status = EXIT_SUCCESS;

    if (argc < 3) {
        fprintf(stderr, "Usage: %s <in> <out>", argv[0]);
        return EXIT_FAILURE;
    }
    
    enum bmp_read_status read_status;
    enum bmp_write_status write_status;

    struct image source;
    if ((read_status = from_bmp_path(argv[1], &source)) != BMP_READ_OK) {
        fprintf(stderr, "Read error: %s.\n", bmp_read_status_message(read_status));
        status = EXIT_FAILURE;
        goto exit;
    }

    struct image result = rotate(&source);
    if (result.data == NULL) {
        fprintf(stderr, "Out of memory.\n");
        status = EXIT_FAILURE;
        goto exit;
    }

    if ((write_status = to_bmp_path(argv[2], &result)) != BMP_WRITE_OK) {
        fprintf(stderr, "Write error: %s.\n", bmp_write_status_message(write_status));
        status = EXIT_FAILURE;
        goto exit;
    }
    
exit:
    image_free(&source);
    image_free(&result);
    return status;
}
