#pragma once

#include <stdint.h>
#include <stdio.h>

#include "image.h"

enum bmp_read_status {
    BMP_READ_OK = 0,
    BMP_READ_ERROR_IO,
    BMP_READ_ERROR_OOM,
    BMP_READ_ERROR_INVALID_SIGNATURE,
    BMP_READ_ERROR_UNSUPPORTED,
    BMP_READ_ERROR_UNEXPECTED_EOF,
};

char *bmp_read_status_message(enum bmp_read_status status);

enum bmp_read_status from_bmp(FILE *file, struct image *image);

enum bmp_read_status from_bmp_path(char *path, struct image *image);

enum bmp_write_status  {
    BMP_WRITE_OK = 0,
    BMP_WRITE_ERROR_IO,
};

char *bmp_write_status_message(enum bmp_write_status status);

enum bmp_write_status to_bmp(FILE *file, struct image const* image);

enum bmp_write_status to_bmp_path(char *path, struct image const* image);

